﻿namespace UD5T1
{
    public partial class MainPage : ContentPage
    {
        decimal cuenta;      // Variable para almacenar el monto de la cuenta
        int propina;         // Variable para almacenar el porcentaje de propina
        int personas = 1;    // Variable para almacenar el número de personas, inicializado en 1 por defecto

        public MainPage()
        {
            InitializeComponent(); // Método de inicialización de la página
        }

        // Método para calcular y actualizar los totales
        public void CalcularTotal()
        {
            // Calcula la propina total y la propina por persona
            var propinaTotal = cuenta * propina / 100;
            var propinaPorPersona = propinaTotal / personas;

            // Actualiza las etiquetas de la interfaz de usuario con los resultados calculados
            lblPropinaPorPersona.Text = $"{propinaPorPersona:C}";
            var subtotal = cuenta / personas;
            lblSubtotal.Text = $"{subtotal:C}";
            var totalPorPersona = (cuenta + propinaTotal) / personas;
            lblTotal.Text = $"{totalPorPersona:C}";
        }

        // Manejador de evento cuando se completa la entrada de texto de la cuenta
        private void TxtCuenta_Completed(object sender, EventArgs e)
        {
            cuenta = decimal.Parse(txtCuenta.Text); // Convierte la entrada de texto a decimal y actualiza la variable de cuenta
            CalcularTotal(); // Llama al método para calcular y actualizar los totales
        }

        // Manejador de evento cuando se cambia el valor del control deslizante de propina
        private void SldPropina_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            propina = ((int)sldPropina.Value); // Actualiza la variable de propina con el nuevo valor del control deslizante
            lblPropina.Text = "Propina: " + propina.ToString() + "%"; // Actualiza la etiqueta de propina en la interfaz de usuario
            CalcularTotal(); // Llama al método para calcular y actualizar los totales
        }

        // Manejador de evento cuando se hace clic en un botón de porcentaje personalizado
        private void Button_Clicked(object sender, EventArgs e)
        {
            if (sender is Button)
            {
                var btn = (Button)sender; // Convierte el objeto de sender a un botón
                var porcentaje = int.Parse(btn.Text.Replace("%", string.Empty)); // Extrae el porcentaje del texto del botón
                sldPropina.Value = porcentaje; // Actualiza el valor del control deslizante de propina
            }
        }

        // Manejador de evento cuando se hace clic en el botón de reducir el número de personas
        private void BtnMenos_Clicked(object sender, EventArgs e)
        {
            if (personas > 1)
            {
                personas--; // Reduce el número de personas si es mayor que 1
                lblPersonas.Text = $"{personas}"; // Actualiza la etiqueta de personas en la interfaz de usuario
                CalcularTotal(); // Llama al método para calcular y actualizar los totales
            }
        }

        // Manejador de evento cuando se hace clic en el botón de aumentar el número de personas
        private void BtnMas_Clicked(object sender, EventArgs e)
        {
            personas++; // Aumenta el número de personas
            lblPersonas.Text = $"{personas}"; // Actualiza la etiqueta de personas en la interfaz de usuario
            CalcularTotal(); // Llama al método para calcular y actualizar los totales
        }
    }

}